package cointracker;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import org.json.simple.parser.ParseException;

public class CoinScrumController {
    
    MainCryptoList fullList = new MainCryptoList();
    
    //List to hold Combo options
    ObservableList<String> sorts = 
    FXCollections.observableArrayList(
        "name",
        "ticker",
        "volume",
        "price",
        "supply",
        "change12",
        "change24",
        "change3",
        "change7",
        "changemonth"
    );
    String sortChoice = "";
    
    
    //int to hold what page we are on
    int pageNum = 0;
   
    //Arraylist for display purposes
    ArrayList<Crypto> display = new ArrayList<>();
    
    //String to hold textbox on main page
    String coinText = "";
    
    //Investment crypto 
    InvestCrypto invested = new InvestCrypto();
    
    
    
    //List to hold and read in investments
    CryptoList investments = new CryptoList();
    
    BigDecimal PriceChange = new BigDecimal(0.0);
    
    
    //Favorites list
    Favorites favorites = new Favorites();
    
    
    //List for amount choice
    ObservableList<Integer> amounts = 
    FXCollections.observableArrayList(
        1,2,3,4,5,6,7,8,9,10
    );
    
    int amountChoice = 0;

    @FXML
    private ComboBox<Integer> amountComboBox;
    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML
    private Tab CoinsTab;
    @FXML
    private Button InvestButton;
    @FXML
    private TextField enterNumTextField;
    @FXML
    private Button AddToFavoritesButton;
    @FXML
    private Button newsButton;
    @FXML
    private Button NextButton;
    @FXML
    private Button PreviousButton;
    @FXML
    private Button SortButton;
    @FXML
    private ComboBox<String> SortingComboBox;
    @FXML
    private Button CoinButton2;
    @FXML
    private Button CoinButton4;
    @FXML
    private Button CoinButton3;
    @FXML
    private Button CoinButton5;
    @FXML
    private Button CoinButton6;
    @FXML
    private Button CoinButton7;
    @FXML
    private Button CoinButton8;
    @FXML
    private Button CoinButton9;

    @FXML
    private Button CoinButton10;

    @FXML
    private Button CoinButton1;

    @FXML
    private Tab FavoritesTab;

    @FXML
    private Button FavoritesRemoveButton;

    @FXML
    private TextField enterNumTextField1;

    @FXML
    private Button FavoritesAddButton1;

    @FXML
    private TextField FavoritesTextField1;

    @FXML
    private TextField FavoritesTextField2;

    @FXML
    private TextField FavoritesTextField3;

    @FXML
    private TextField FavoritesTextField4;

    @FXML
    private TextField FavoritesTextField5;

    @FXML
    private TextField FavoritesTextField6;

    @FXML
    private TextField FavoritesTextField7;

    @FXML
    private TextField FavoritesTextField8;

    @FXML
    private TextField FavoritesTextField9;

    @FXML
    private TextField FavoritesTextField10;

    @FXML
    private Tab InvestmentsTab;

    @FXML
    private Button InvestmentsRemoveButton;

    @FXML
    private TextField enterNumTextField2;

    @FXML
    private Button InvestmentsAddButton;

    @FXML
    private TextField InvestmentsTextField1;

    @FXML
    private TextField InvestmentsTextField2;

    @FXML
    private TextField InvestmentsTextField3;

    @FXML
    private TextField InvestmentsTextField4;

    @FXML
    private TextField InvestmentsTextField5;

    @FXML
    private TextField InvestmentsTextField6;

    @FXML
    private TextField InvestmentsTextField7;

    @FXML
    private TextField InvestmentsTextField8;

    @FXML
    private TextField InvestmentsTextField9;

    @FXML
    private TextField InvestmentsTextField10;

    @FXML
    private Tab InformationTab;

    @FXML
    private TextField InfoTextField1;

    @FXML
    private TextField InfoTextField2;

    @FXML
    private TextField InfoTextField3;

    @FXML
    private TextField InfoTextField4;

    @FXML
    private TextField InfoTextField5;

    @FXML
    private TextField InfoTextField6;

    @FXML
    private TextField InfoTextField7;

    @FXML
    private TextField InfoTextField8;

    @FXML
    private TextField InfoTextField9;

    @FXML
    private TextField InfoTextField10;

    
    @FXML
    void AddToFavoritesButtonPressed(ActionEvent event) throws IOException {
        coinText = enterNumTextField.getText();
        //the user leaves the field empty do nothing
        if(!(coinText.equals(""))){
            if(!(fullList.indexOf(coinText)== -1)){
                favorites.add(fullList.get(fullList.indexOf(coinText)));
                favorites.writeToFile();
                displayFavorites();
            }
        }
    }
    
    public void displayFavorites(){
        for(int i = 0; i < favorites.getFaves().size(); i++){
         
            switch(i){
                case 0:
                    FavoritesTextField1.setText(favorites.getFaves().get(i).toString());
                    break;
                case 1:
                    FavoritesTextField2.setText(favorites.getFaves().get(i).toString());
                    break;
                case 2:
                    FavoritesTextField3.setText(favorites.getFaves().get(i).toString());
                    break;
                case 3:
                    FavoritesTextField4.setText(favorites.getFaves().get(i).toString());
                    break;
                case 4:
                    FavoritesTextField5.setText(favorites.getFaves().get(i).toString());
                    break;
                case 5:
                    FavoritesTextField6.setText(favorites.getFaves().get(i).toString());
                    break;
                case 6:
                    FavoritesTextField7.setText(favorites.getFaves().get(i).toString());
                    break;
                case 7:
                    FavoritesTextField8.setText(favorites.getFaves().get(i).toString());
                    break;
                case 8:
                    FavoritesTextField9.setText(favorites.getFaves().get(i).toString());
                    break;
                case 9:
                    FavoritesTextField10.setText(favorites.getFaves().get(i).toString());
                    break;
                default:
            }
        }
    }

    @FXML
    void CoinButton10Pressed(ActionEvent event) {
        InfoTextField1.setText(display.get(9).ticker);
        InfoTextField2.setText(display.get(9).name);
        InfoTextField3.setText(display.get(9).price.toString());
        InfoTextField4.setText(display.get(9).volume.toString());
        InfoTextField5.setText(Double.toString(display.get(9).supply));
        InfoTextField6.setText(Double.toString(display.get(9).change12));
        InfoTextField7.setText(Double.toString(display.get(9).change24));
        InfoTextField8.setText(Double.toString(display.get(9).change3));
        InfoTextField9.setText(Double.toString(display.get(9).change7));
        InfoTextField10.setText(Double.toString(display.get(9).changemonth));
    }

    @FXML
    void CoinButton1Pressed(ActionEvent event) {
        InfoTextField1.setText(display.get(0).ticker);
        InfoTextField2.setText(display.get(0).name);
        InfoTextField3.setText(display.get(0).price.toString());
        InfoTextField4.setText(display.get(0).volume.toString());
        InfoTextField5.setText(Double.toString(display.get(0).supply));
        InfoTextField6.setText(Double.toString(display.get(0).change12));
        InfoTextField7.setText(Double.toString(display.get(0).change24));
        InfoTextField8.setText(Double.toString(display.get(0).change3));
        InfoTextField9.setText(Double.toString(display.get(0).change7));
        InfoTextField10.setText(Double.toString(display.get(0).changemonth));
        
    }

    @FXML
    void CoinButton2Pressed(ActionEvent event) {
        InfoTextField1.setText(display.get(1).ticker);
        InfoTextField2.setText(display.get(1).name);
        InfoTextField3.setText(display.get(1).price.toString());
        InfoTextField4.setText(display.get(1).volume.toString());
        InfoTextField5.setText(Double.toString(display.get(1).supply));
        InfoTextField6.setText(Double.toString(display.get(1).change12));
        InfoTextField7.setText(Double.toString(display.get(1).change24));
        InfoTextField8.setText(Double.toString(display.get(1).change3));
        InfoTextField9.setText(Double.toString(display.get(1).change7));
        InfoTextField10.setText(Double.toString(display.get(1).changemonth));
    }

    @FXML
    void CoinButton3Pressed(ActionEvent event) {
        InfoTextField1.setText(display.get(2).ticker);
        InfoTextField2.setText(display.get(2).name);
        InfoTextField3.setText(display.get(2).price.toString());
        InfoTextField4.setText(display.get(2).volume.toString());
        InfoTextField5.setText(Double.toString(display.get(2).supply));
        InfoTextField6.setText(Double.toString(display.get(2).change12));
        InfoTextField7.setText(Double.toString(display.get(2).change24));
        InfoTextField8.setText(Double.toString(display.get(2).change3));
        InfoTextField9.setText(Double.toString(display.get(2).change7));
        InfoTextField10.setText(Double.toString(display.get(2).changemonth));
    }

    @FXML
    void CoinButton4Pressed(ActionEvent event) {
        InfoTextField1.setText(display.get(3).ticker);
        InfoTextField2.setText(display.get(3).name);
        InfoTextField3.setText(display.get(3).price.toString());
        InfoTextField4.setText(display.get(3).volume.toString());
        InfoTextField5.setText(Double.toString(display.get(3).supply));
        InfoTextField6.setText(Double.toString(display.get(3).change12));
        InfoTextField7.setText(Double.toString(display.get(3).change24));
        InfoTextField8.setText(Double.toString(display.get(3).change3));
        InfoTextField9.setText(Double.toString(display.get(3).change7));
        InfoTextField10.setText(Double.toString(display.get(3).changemonth));
    }

    @FXML
    void CoinButton5Pressed(ActionEvent event) {
        InfoTextField1.setText(display.get(4).ticker);
        InfoTextField2.setText(display.get(4).name);
        InfoTextField3.setText(display.get(4).price.toString());
        InfoTextField4.setText(display.get(4).volume.toString());
        InfoTextField5.setText(Double.toString(display.get(4).supply));
        InfoTextField6.setText(Double.toString(display.get(4).change12));
        InfoTextField7.setText(Double.toString(display.get(4).change24));
        InfoTextField8.setText(Double.toString(display.get(4).change3));
        InfoTextField9.setText(Double.toString(display.get(4).change7));
        InfoTextField10.setText(Double.toString(display.get(4).changemonth));
    }

    @FXML
    void CoinButton6Pressed(ActionEvent event) {
        InfoTextField1.setText(display.get(5).ticker);
        InfoTextField2.setText(display.get(5).name);
        InfoTextField3.setText(display.get(5).price.toString());
        InfoTextField4.setText(display.get(5).volume.toString());
        InfoTextField5.setText(Double.toString(display.get(5).supply));
        InfoTextField6.setText(Double.toString(display.get(5).change12));
        InfoTextField7.setText(Double.toString(display.get(5).change24));
        InfoTextField8.setText(Double.toString(display.get(5).change3));
        InfoTextField9.setText(Double.toString(display.get(5).change7));
        InfoTextField10.setText(Double.toString(display.get(5).changemonth));
    }

    @FXML
    void CoinButton7Pressed(ActionEvent event) {
        InfoTextField1.setText(display.get(6).ticker);
        InfoTextField2.setText(display.get(6).name);
        InfoTextField3.setText(display.get(6).price.toString());
        InfoTextField4.setText(display.get(6).volume.toString());
        InfoTextField5.setText(Double.toString(display.get(6).supply));
        InfoTextField6.setText(Double.toString(display.get(6).change12));
        InfoTextField7.setText(Double.toString(display.get(6).change24));
        InfoTextField8.setText(Double.toString(display.get(6).change3));
        InfoTextField9.setText(Double.toString(display.get(6).change7));
        InfoTextField10.setText(Double.toString(display.get(6).changemonth));
    }

    @FXML
    void CoinButton8Pressed(ActionEvent event) {
        InfoTextField1.setText(display.get(7).ticker);
        InfoTextField2.setText(display.get(7).name);
        InfoTextField3.setText(display.get(7).price.toString());
        InfoTextField4.setText(display.get(7).volume.toString());
        InfoTextField5.setText(Double.toString(display.get(7).supply));
        InfoTextField6.setText(Double.toString(display.get(7).change12));
        InfoTextField7.setText(Double.toString(display.get(7).change24));
        InfoTextField8.setText(Double.toString(display.get(7).change3));
        InfoTextField9.setText(Double.toString(display.get(7).change7));
        InfoTextField10.setText(Double.toString(display.get(7).changemonth));
    }

    @FXML
    void CoinButton9Pressed(ActionEvent event) {
        InfoTextField1.setText(display.get(8).ticker);
        InfoTextField2.setText(display.get(8).name);
        InfoTextField3.setText(display.get(8).price.toString());
        InfoTextField4.setText(display.get(8).volume.toString());
        InfoTextField5.setText(Double.toString(display.get(8).supply));
        InfoTextField6.setText(Double.toString(display.get(8).change12));
        InfoTextField7.setText(Double.toString(display.get(8).change24));
        InfoTextField8.setText(Double.toString(display.get(8).change3));
        InfoTextField9.setText(Double.toString(display.get(8).change7));
        InfoTextField10.setText(Double.toString(display.get(8).changemonth));
    }

    @FXML
    void CoinsTabChanged(ActionEvent event) {

    }

    @FXML
    void FavoritesAddButtonPressed(ActionEvent event) throws IOException{
        
        coinText = enterNumTextField1.getText();
        //the user leaves the field empty do nothing
        if(!(coinText.equals(""))){
            if(!(fullList.indexOf(coinText)== -1)){
                favorites.add(fullList.get(fullList.indexOf(coinText)));
                favorites.writeToFile();
                displayFavorites();
            }
        }
    }

    @FXML
    void FavoritesRemoveButtonPressed(ActionEvent event) throws IOException {
        coinText = enterNumTextField1.getText();
        if(!(coinText.equals("")) && !(favorites.getFaves().isEmpty())){
            for(int i = 0; i < favorites.getFaves().size(); i++){
                if(coinText.equals(favorites.getFaves().get(i).ticker)){
                    favorites.getFaves().remove(i);
                    favorites.writeToFile();
                    FavoritesTextField1.setText(""); FavoritesTextField2.setText(""); FavoritesTextField3.setText(""); 
                    FavoritesTextField4.setText(""); FavoritesTextField5.setText(""); FavoritesTextField6.setText(""); 
                    FavoritesTextField7.setText(""); FavoritesTextField8.setText(""); FavoritesTextField9.setText(""); 
                    FavoritesTextField10.setText(""); 
                    displayFavorites();
                    
                }
            }
        }
        
    }

    @FXML
    void FavoritesTabChanged(ActionEvent event) {

    }

    @FXML
    void InformationTabChanged(ActionEvent event) {

    }

    @FXML
    void InvestButtonPressed(ActionEvent event) throws IOException {
        
        for(int i = 0; i < investments.crypList.size(); i++){
            investments.crypList.get(i).coin.price = fullList.get(fullList.indexOf(investments.crypList.get(i).coin.ticker)).price;
        }
        
        coinText = enterNumTextField.getText();
        //the user leaves the field empty do nothing
        if(!(coinText.equals(""))){
            if(!(fullList.indexOf(coinText)== -1)){
                invested.coin = fullList.get(fullList.indexOf(coinText));
                //Default amount be 1
                invested.amount = 1;
                invested.CurrentPrice = fullList.get(fullList.indexOf(coinText)).price;
                //add it to the list
                investments.add(new InvestCrypto(invested));
                investments.fileOut();
                displayInvestments();
            }
        }
        
    }

    @FXML
    void InvestmentsAddButtonPressed(ActionEvent event) throws IOException {
        for(int i = 0; i < investments.crypList.size(); i++){
            investments.crypList.get(i).coin.price = fullList.get(fullList.indexOf(investments.crypList.get(i).coin.ticker)).price;
        }
        coinText = enterNumTextField2.getText();
        //the user leaves the field empty do nothing
        
            if(!(enterNumTextField2.equals("")) && amountChoice > 0) {
                if(!(fullList.indexOf(coinText)== -1)){
                    invested.coin = fullList.get(fullList.indexOf(coinText));
                    //Add the amount selected in the combo box
                    invested.amount = amountChoice;
                    invested.CurrentPrice = fullList.get(fullList.indexOf(coinText)).price;
                    //add it to the list
                    investments.add(new InvestCrypto(invested));
                    investments.fileOut();
                    displayInvestments();
                }
            }
    }

    @FXML
    void InvestmentsRemoveButtonPressed(ActionEvent event) throws IOException {
        
        for(int i = 0; i < investments.crypList.size(); i++){
            investments.crypList.get(i).coin.price = fullList.get(fullList.indexOf(investments.crypList.get(i).coin.ticker)).price;
        }
        
        coinText = enterNumTextField2.getText();
        //the user leaves the field empty do nothing
        if(!(enterNumTextField2.equals("")) && amountChoice > 0) {
            if(!investments.crypList.isEmpty()){
                for(int i = 0; i < investments.crypList.size(); i++){
                    if(investments.crypList.get(i).coin.ticker.equals(coinText)){
                        if(investments.crypList.get(i).amount > 1){
                            investments.crypList.get(i).amount -= amountChoice;
                            
                            if(investments.crypList.get(i).amount <= 0){
                                investments.crypList.remove(i);
                                investments.fileOut();
                                InvestmentsTextField1.setText(""); InvestmentsTextField2.setText(""); InvestmentsTextField3.setText(""); 
                                InvestmentsTextField4.setText(""); InvestmentsTextField5.setText(""); InvestmentsTextField6.setText(""); 
                                InvestmentsTextField7.setText(""); InvestmentsTextField8.setText(""); InvestmentsTextField9.setText(""); 
                                InvestmentsTextField10.setText("");
                                displayInvestments();
                            }
                            
                            investments.fileOut();
                            InvestmentsTextField1.setText(""); InvestmentsTextField2.setText(""); InvestmentsTextField3.setText(""); 
                            InvestmentsTextField4.setText(""); InvestmentsTextField5.setText(""); InvestmentsTextField6.setText(""); 
                            InvestmentsTextField7.setText(""); InvestmentsTextField8.setText(""); InvestmentsTextField9.setText(""); 
                            InvestmentsTextField10.setText("");
                            displayInvestments();
                            break;
                        }else{
                        
                        investments.crypList.remove(i);
                        investments.fileOut();
                        InvestmentsTextField1.setText(""); InvestmentsTextField2.setText(""); InvestmentsTextField3.setText(""); 
                        InvestmentsTextField4.setText(""); InvestmentsTextField5.setText(""); InvestmentsTextField6.setText(""); 
                        InvestmentsTextField7.setText(""); InvestmentsTextField8.setText(""); InvestmentsTextField9.setText(""); 
                        InvestmentsTextField10.setText("");
                        displayInvestments();
                    }
            }
        }
    }
        }}    
    
    
    
    
    public void displayInvestments(){
        Double priceC = 0.0;
        for(int i = 0; i < investments.crypList.size(); i++){
         
            switch(i){
                case 0:
                    PriceChange = fullList.get(fullList.indexOf(investments.crypList.get(i).coin.ticker)).price.subtract(
                            investments.crypList.get(i).CurrentPrice);
                    PriceChange = PriceChange.multiply(new BigDecimal(investments.crypList.get(i).amount));
                            priceC = Double.valueOf(PriceChange.toString());
                    InvestmentsTextField1.setText(investments.crypList.get(i).toString() + String.format("%.2f", priceC));
                    break;
                case 1:
                    PriceChange = fullList.get(fullList.indexOf(investments.crypList.get(i).coin.ticker)).price.subtract(
                            investments.crypList.get(i).CurrentPrice);
                    PriceChange = PriceChange.multiply(new BigDecimal(investments.crypList.get(i).amount));
                         priceC = Double.valueOf(PriceChange.toString());
                    InvestmentsTextField2.setText(investments.crypList.get(i).toString() + String.format("%.2f", priceC));
                    break;
                case 2:
                    PriceChange = fullList.get(fullList.indexOf(investments.crypList.get(i).coin.ticker)).price.subtract(
                            investments.crypList.get(i).CurrentPrice);
                    PriceChange = PriceChange.multiply(new BigDecimal(investments.crypList.get(i).amount));
                         priceC = Double.valueOf(PriceChange.toString());
                    InvestmentsTextField3.setText(investments.crypList.get(i).toString() +String.format("%.2f", priceC));
                    break;
                case 3:
                    PriceChange = fullList.get(fullList.indexOf(investments.crypList.get(i).coin.ticker)).price.subtract(
                            investments.crypList.get(i).CurrentPrice);
                    PriceChange = PriceChange.multiply(new BigDecimal(investments.crypList.get(i).amount));
                         priceC = Double.valueOf(PriceChange.toString());
                    InvestmentsTextField4.setText(investments.crypList.get(i).toString() + String.format("%.2f", priceC));
                    break;
                case 4:
                    PriceChange = fullList.get(fullList.indexOf(investments.crypList.get(i).coin.ticker)).price.subtract(
                            investments.crypList.get(i).CurrentPrice);
                    PriceChange = PriceChange.multiply(new BigDecimal(investments.crypList.get(i).amount));
                         priceC = Double.valueOf(PriceChange.toString());
                    InvestmentsTextField5.setText(investments.crypList.get(i).toString() + String.format("%.2f", priceC));
                    break;
                case 5:
                    PriceChange = fullList.get(fullList.indexOf(investments.crypList.get(i).coin.ticker)).price.subtract(
                            investments.crypList.get(i).CurrentPrice);
                    PriceChange = PriceChange.multiply(new BigDecimal(investments.crypList.get(i).amount));
                         priceC = Double.valueOf(PriceChange.toString());
                    InvestmentsTextField6.setText(investments.crypList.get(i).toString() + String.format("%.2f", priceC));
                    break;
                case 6:
                    PriceChange = fullList.get(fullList.indexOf(investments.crypList.get(i).coin.ticker)).price.subtract(
                            investments.crypList.get(i).CurrentPrice);
                    PriceChange = PriceChange.multiply(new BigDecimal(investments.crypList.get(i).amount));
                         priceC = Double.valueOf(PriceChange.toString());
                    InvestmentsTextField7.setText(investments.crypList.get(i).toString() + String.format("%.2f", priceC));
                    break;
                case 7:
                    PriceChange = fullList.get(fullList.indexOf(investments.crypList.get(i).coin.ticker)).price.subtract(
                            investments.crypList.get(i).CurrentPrice);
                    PriceChange = PriceChange.multiply(new BigDecimal(investments.crypList.get(i).amount));
                         priceC = Double.valueOf(PriceChange.toString());
                    InvestmentsTextField8.setText(investments.crypList.get(i).toString() + String.format("%.2f", priceC));
                    break;
                case 8:
                    PriceChange = fullList.get(fullList.indexOf(investments.crypList.get(i).coin.ticker)).price.subtract(
                            investments.crypList.get(i).CurrentPrice);
                    PriceChange = PriceChange.multiply(new BigDecimal(investments.crypList.get(i).amount));
                         priceC = Double.valueOf(PriceChange.toString());
                    InvestmentsTextField9.setText(investments.crypList.get(i).toString() + String.format("%.2f", priceC));
                    break;
                case 9:
                    PriceChange = fullList.get(fullList.indexOf(investments.crypList.get(i).coin.ticker)).price.subtract(
                            investments.crypList.get(i).CurrentPrice);
                    PriceChange = PriceChange.multiply(new BigDecimal(investments.crypList.get(i).amount));
                         priceC = Double.valueOf(PriceChange.toString());
                    InvestmentsTextField10.setText(investments.crypList.get(i).toString() + String.format("%.2f", priceC));
                    break;
                default:
            }
        }
    }

    @FXML
    void InvestmentsTabChanged(ActionEvent event) {

    }

    @FXML
    void NextButtonPressed(ActionEvent event) {
        
        if(pageNum < 9){
        pageNum = pageNum + 1;

        display.clear();
        //set the display to the next 10 coins in the list
        for(int i = pageNum * 10; i < (pageNum + 1) * 10; i++){
            display.add(fullList.get(i));
        }
        
        displayCryptos();
        }
    }

    @FXML
    void PreviousButtonPressed(ActionEvent event) {
        pageNum = pageNum - 1;

        if(pageNum <= 0){
            pageNum = 0;
            display.clear();
             //set the display to the next 10 coins in the list
            for(int i = 0; i < 10; i++){
                display.add(fullList.get(i));
            }
        } else {
            display.clear();
            //set the display to the next 10 coins in the list
            for(int i = (pageNum) * 10; i < (pageNum) * 10 + 10; i++){
                display.add(fullList.get(i));
            }
        }
        
        
        displayCryptos();
    }

    @FXML
    void SortButtonPressed(ActionEvent event) {
        if(!sortChoice.equals("")){
            MainCryptoList.Sort(sortChoice);
        
        display.clear();
        for(int i = 0; i < 10; i++){
            display.add(fullList.get(i));
        }
        pageNum = 0;
        displayCryptos();
        }
    }

    @FXML
    void SortingComboBoxPressed(ActionEvent event) {

    }
    
    @FXML
    void AmountClicked(ActionEvent event) {

    }

    @FXML
    void newButtonPressed(ActionEvent event) throws URISyntaxException, IOException {
        java.awt.Desktop.getDesktop().browse(new java.net.URI("https://cointelegraph.com/tags/cryptocurrencies"));
    }

    @FXML
    void initialize() throws IOException, ParseException {
        assert CoinsTab != null : "fx:id=\"CoinsTab\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InvestButton != null : "fx:id=\"InvestButton\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert enterNumTextField != null : "fx:id=\"enterNumTextField\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert AddToFavoritesButton != null : "fx:id=\"AddToFavoritesButton\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert newsButton != null : "fx:id=\"newsButton\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert NextButton != null : "fx:id=\"NextButton\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert PreviousButton != null : "fx:id=\"PreviousButton\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert SortButton != null : "fx:id=\"SortButton\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert SortingComboBox != null : "fx:id=\"SortingComboBox\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert CoinButton2 != null : "fx:id=\"CoinButton2\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert CoinButton4 != null : "fx:id=\"CoinButton4\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert CoinButton3 != null : "fx:id=\"CoinButton3\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert CoinButton5 != null : "fx:id=\"CoinButton5\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert CoinButton6 != null : "fx:id=\"CoinButton6\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert CoinButton7 != null : "fx:id=\"CoinButton7\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert CoinButton8 != null : "fx:id=\"CoinButton8\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert CoinButton9 != null : "fx:id=\"CoinButton9\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert CoinButton10 != null : "fx:id=\"CoinButton10\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert CoinButton1 != null : "fx:id=\"CoinButton1\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert FavoritesTab != null : "fx:id=\"FavoritesTab\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert FavoritesRemoveButton != null : "fx:id=\"FavoritesRemoveButton\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert enterNumTextField1 != null : "fx:id=\"enterNumTextField1\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert FavoritesAddButton1 != null : "fx:id=\"FavoritesAddButton1\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert FavoritesTextField1 != null : "fx:id=\"FavoritesTextField1\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert FavoritesTextField2 != null : "fx:id=\"FavoritesTextField2\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert FavoritesTextField3 != null : "fx:id=\"FavoritesTextField3\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert FavoritesTextField4 != null : "fx:id=\"FavoritesTextField4\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert FavoritesTextField5 != null : "fx:id=\"FavoritesTextField5\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert FavoritesTextField6 != null : "fx:id=\"FavoritesTextField6\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert FavoritesTextField7 != null : "fx:id=\"FavoritesTextField7\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert FavoritesTextField8 != null : "fx:id=\"FavoritesTextField8\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert FavoritesTextField9 != null : "fx:id=\"FavoritesTextField9\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert FavoritesTextField10 != null : "fx:id=\"FavoritesTextField10\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InvestmentsTab != null : "fx:id=\"InvestmentsTab\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InvestmentsRemoveButton != null : "fx:id=\"InvestmentsRemoveButton\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert enterNumTextField2 != null : "fx:id=\"enterNumTextField2\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InvestmentsAddButton != null : "fx:id=\"InvestmentsAddButton\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InvestmentsTextField1 != null : "fx:id=\"InvestmentsTextField1\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InvestmentsTextField2 != null : "fx:id=\"InvestmentsTextField2\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InvestmentsTextField3 != null : "fx:id=\"InvestmentsTextField3\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InvestmentsTextField4 != null : "fx:id=\"InvestmentsTextField4\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InvestmentsTextField5 != null : "fx:id=\"InvestmentsTextField5\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InvestmentsTextField6 != null : "fx:id=\"InvestmentsTextField6\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InvestmentsTextField7 != null : "fx:id=\"InvestmentsTextField7\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InvestmentsTextField8 != null : "fx:id=\"InvestmentsTextField8\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InvestmentsTextField9 != null : "fx:id=\"InvestmentsTextField9\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InvestmentsTextField10 != null : "fx:id=\"InvestmentsTextField10\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InformationTab != null : "fx:id=\"InformationTab\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InfoTextField1 != null : "fx:id=\"InfoTextField1\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InfoTextField2 != null : "fx:id=\"InfoTextField2\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InfoTextField3 != null : "fx:id=\"InfoTextField3\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InfoTextField4 != null : "fx:id=\"InfoTextField4\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InfoTextField5 != null : "fx:id=\"InfoTextField5\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InfoTextField6 != null : "fx:id=\"InfoTextField6\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InfoTextField7 != null : "fx:id=\"InfoTextField7\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InfoTextField8 != null : "fx:id=\"InfoTextField8\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InfoTextField9 != null : "fx:id=\"InfoTextField9\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        assert InfoTextField10 != null : "fx:id=\"InfoTextField10\" was not injected: check your FXML file 'CoinScrum3TabsFXML (1).fxml'.";
        
        
        
        fullList.readIn();
    
        //Setting up the sort combo box
    SortingComboBox.setItems(sorts);
    SortingComboBox.valueProperty().addListener(new ChangeListener<String>() {
                        @Override 
			public void changed(ObservableValue<? extends String> ov, String oldValue, String newValue) {
				sortChoice = newValue;
			}    
		}
		);
    
    
    
    amountComboBox.setItems(amounts);
    amountComboBox.valueProperty().addListener(new ChangeListener<Integer>() {
                        @Override 
			public void changed(ObservableValue<? extends Integer> ov, Integer oldValue, Integer newValue) {
				amountChoice = newValue;
			}    
		}
		);
    //Load the first 10 entries into the display arraylist
    for(int i = 0; i < 10; i++){
        display.add(fullList.get(i));
    }
    
    displayCryptos();
    pageNum = 0;
        
    favorites.readFromFile();
    displayFavorites();
    
    investments.filein();
    displayInvestments();
    
    for(int i = 0; i < investments.crypList.size(); i++){
            investments.crypList.get(i).coin.price = fullList.get(fullList.indexOf(investments.crypList.get(i).coin.ticker)).price;
    }
    }
    
    public void displayCryptos(){
        for(int i = 0; i < display.size(); i++){
         
            switch(i){
                case 0:
                    CoinButton1.setText(display.get(i).toString());
                    break;
                case 1:
                    CoinButton2.setText(display.get(i).toString());
                    break;
                case 2:
                    CoinButton3.setText(display.get(i).toString());
                    break;
                case 3:
                    CoinButton4.setText(display.get(i).toString());
                    break;
                case 4:
                    CoinButton5.setText(display.get(i).toString());
                    break;
                case 5:
                    CoinButton6.setText(display.get(i).toString());
                    break;
                case 6:
                    CoinButton7.setText(display.get(i).toString());
                    break;
                case 7:
                    CoinButton8.setText(display.get(i).toString());
                    break;
                case 8:
                    CoinButton9.setText(display.get(i).toString());
                    break;
                case 9:
                    CoinButton10.setText(display.get(i).toString());
                    break;
                default:
            }
        }
    }
    
    
}
