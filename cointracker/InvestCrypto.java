/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cointracker;

import java.math.BigDecimal;
import java.util.UUID;

/**
 *
 * @author logan
 */
public class InvestCrypto 
{
    String PurchID;
    Crypto coin;
    int amount;
    BigDecimal CurrentPrice;
    
    InvestCrypto()
    {
        PurchID =  UUID.randomUUID().toString();
        coin = new Crypto();
        amount = 0;
        CurrentPrice = new BigDecimal(0.0);
    }
    
    InvestCrypto(Crypto c, int a)
    {
        PurchID = UUID.randomUUID().toString();
        coin = c;
        amount = a;
        CurrentPrice = new BigDecimal(0.0);
    }
    
    InvestCrypto(InvestCrypto c){
        PurchID = c.PurchID;
        coin = c.coin;
        amount = c.amount;
        CurrentPrice = c.CurrentPrice;
    }
    
    public String toString(){
        String s = "";
        s = coin.ticker + "     " + coin.name + "       " + String.format("%.2f", CurrentPrice) + "        " + amount + "      ";
        return s;
    }
    
    
    public String getID() {
        return PurchID;
    }

    public void setID(String ID) {
        this.PurchID = ID;
    }

    public Crypto getCoin() {
        return coin;
    }

    public void setCoin(Crypto coin) {
        this.coin = coin;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    
    
    
    
}
