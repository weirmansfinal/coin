/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cointracker;

import java.math.BigDecimal;

/**
 *
 * @author Andrew Gray
 * 
 */
public class Crypto {
    //Variable to store the currency's name
    String name;
    
    //Variable to store the currency's ticker
    String ticker;
    
    //Variable to store the Trade Volume of each currency
    BigDecimal volume;
    
    //Variable to hold price of a single currency
    BigDecimal price;
    
    //Variable to store the total amount of units are out there
    double supply;
    
    //percentage of change within 12 hours
    double change12;
    
    //percentage of change within 24 hours
    double change24;
    
    //percentage of change within 3 days
    double change3;
    
    //percentage of change within 7 days
    double change7;
    
    //percentage of change within 1 month
    double changemonth;
    
    
    
    //default contructor set everything to null or 0
    public Crypto(){
        name = "";
        ticker = "";
        volume = new BigDecimal(0.0);
        price = new BigDecimal(0.0);
        supply = 0;
        change12 = 0.0;
        change24 = 0.0;
        change3 = 0.0;
        change7 = 0.0;
        changemonth = 0.0;
    }
    
    //Constructer given another crypto
    public Crypto(Crypto c){
        name = c.name;
        ticker = c.ticker;
        volume = c.volume;
        price = c.price;
        supply = c.supply;
        change12 = c.change12;
        change24 = c.change24;
        change3 = c.change3;
        change7 = c.change7;
        changemonth = c.changemonth;
        
    }
    
    public String toString(){
        String s;

        s = name + "    " + ticker  + "     " + String.format("%.2f", volume) + "   " +
                String.format("%.2f", price) + "   " +
                String.format("%.2f", supply) + "    " + 
                String.format("%.2f", change12) + "    " + String.format("%.2f", change24) + "    " +
                String.format("%.2f", change3) + "    " +  String.format("%.2f", change7) +
                "    " + String.format("%.2f", changemonth);
        
        return s;
        
    }
    
    public int compareToName(Crypto o){
        return this.name.compareTo(o.name);
    }
    
    public int compareToTicker(Crypto o){
        return this.ticker.compareTo(o.ticker);
    }
    
    public int compareToVolume(Crypto o){
        return this.volume.compareTo(volume);
    }
    
    public int compareToPrice(Crypto o){
        return this.price.compareTo(o.price);
    }
    
    public int compareToSupply(Crypto o){
        if(this.supply > o.supply){
            return 1;
        }else if(this.supply < o.supply){
            return -1;
        }else{
            return 0;
        }
    }
    
    public int compareToChange12(Crypto o){
        if(this.change12 > o.change12){
            return 1;
        }else if(this.change12 < o.change12){
            return -1;
        }else{
            return 0;
        }
    }
    
    public int compareToChange24(Crypto o){
        if(this.change24 > o.change24){
            return 1;
        }else if(this.change24 < o.change24){
            return -1;
        }else{
            return 0;
        }
    }
    
    public int compareToChange3(Crypto o){
        if(this.change3 > o.change3){
            return 1;
        }else if(this.change3 < o.change3){
            return -1;
        }else{
            return 0;
        }
    }
    
    public int compareToChange7(Crypto o){
        if(this.change7 > o.change7){
            return 1;
        }else if(this.change7 < o.change7){
            return -1;
        }else{
            return 0;
        }
    }
    
    public int compareToChangeMonth(Crypto o){
        if(this.changemonth > o.changemonth){
            return 1;
        }else if(this.changemonth < o.changemonth){
            return -1;
        }else{
            return 0;
        }
    }
    
}
