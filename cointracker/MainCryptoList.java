/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cointracker;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author agray
 */
public class MainCryptoList {

    static ArrayList<Crypto> readIn;

    public MainCryptoList() {
        readIn = new ArrayList<>();
    }

    public void add(Crypto c) {

        if (!readIn.contains(c)) {
            readIn.add(c);
        }
    }

    public void remove(Crypto c) {

        if (readIn.contains(c)) {
            readIn.remove(c);
        }
    }

    public Crypto get(int i) {
        Crypto c = new Crypto();
        c = readIn.get(i);
        return c;
    }

    public String toString() {
        String s = "";

        for (int i = 0; i < readIn.size(); i++) {
            s = s + readIn.get(i).toString() + "\n";

        }

        return s;
    }

    public void readIn() throws IOException, ParseException {
        

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader("C:\\Users\\agray\\OneDrive\\Documents\\NetBeansProjects\\CoinTracker\\data.json"));
        JSONObject jsonObject = (JSONObject) obj;
        JSONArray arr = (JSONArray) jsonObject.get("data");

        for (Object o : arr) {
            Crypto c = new Crypto();
            JSONObject crypto = (JSONObject) o;

            String name = (String) crypto.get("name");
            c.name = name;

            String symbol = (String) crypto.get("symbol");
            c.ticker = symbol;

            double supply = new Double(crypto.get("circulating_supply").toString());
            c.supply = supply;

            JSONObject quotes = (JSONObject) crypto.get("quote");
            JSONObject usd = (JSONObject) quotes.get("USD");

            BigDecimal price = new BigDecimal((double) usd.get("price"));
            c.price = price;

            BigDecimal volume = new BigDecimal(usd.get("volume_24h").toString());
            c.volume = volume;

            double daychange = (double) usd.get("percent_change_24h");
            c.change24 = daychange;

            double weekchange = (double) usd.get("percent_change_7d");
            c.change7 = weekchange;

            readIn.add(new Crypto(c));
        }
        

        JSONParser parser1 = new JSONParser();
        Object obj1 = parser1.parse(new FileReader("C:\\Users\\agray\\OneDrive\\Documents\\NetBeansProjects\\CoinTracker\\12hours.json"));
        JSONArray arr1 = (JSONArray) obj1;
        for (Object o : arr1) {
            JSONObject crypto = (JSONObject) o;
            String name = (String) crypto.get("currency");
            int index = 0;
            index = indexOf(name);
            if(!(index == -1)){
                double price12 = Double.valueOf((String) crypto.get("open"));
                double now = Double.valueOf((String) crypto.get("close"));
                readIn.get(index).change12 = ((now - price12)/ price12) * 100;
            }
        }
        
        
        
        JSONParser parser2 = new JSONParser();
        Object obj2 = parser2.parse(new FileReader("C:\\Users\\agray\\OneDrive\\Documents\\NetBeansProjects\\CoinTracker\\3days.json"));
        JSONArray arr2 = (JSONArray) obj2;
        for (Object o : arr2) {
            JSONObject crypto = (JSONObject) o;
            String name = (String) crypto.get("currency");
            int index = 0;
            index = indexOf(name);
            if(!(index == -1)){
                double price12 = Double.valueOf((String) crypto.get("open"));
                double now = Double.valueOf((String) crypto.get("close"));
                readIn.get(index).change3 = ((now - price12)/ price12) * 100;
            }
        }
        
        
        JSONParser parser3 = new JSONParser();
        Object obj3 = parser3.parse(new FileReader("C:\\Users\\agray\\OneDrive\\Documents\\NetBeansProjects\\CoinTracker\\month.json"));
        JSONArray arr3 = (JSONArray) obj3;
        for (Object o : arr3) {
            JSONObject crypto = (JSONObject) o;
            String name = (String) crypto.get("currency");
            int index = 0;
            index = indexOf(name);
            if(!(index == -1)){
                double price12 = Double.valueOf((String) crypto.get("open"));
                double now = Double.valueOf((String) crypto.get("close"));
                readIn.get(index).changemonth = ((now - price12)/ price12) * 100;
            }
        }
        
    }
    
    
    public int indexOf(String s){
        for(int i = 0; i < readIn.size(); i++){
            if(readIn.get(i).ticker.equals(s)){
                return i;
            }
        }
        return -1;
    }
    
    public boolean isEmpty(){
            return readIn.isEmpty();
    }

    public static void Sort(String a) {
        switch (a) {
            case "name":
                Collections.sort(readIn, new Sortbyname());
                break;
            case "ticker":
                Collections.sort(readIn, new Sortbyticker());
                break;
            case "volume":
                Collections.sort(readIn, new Sortbyvolume());
                break;
            case "price":
                Collections.sort(readIn, new Sortbyprice());
                break;
            case "supply":
                Collections.sort(readIn, new Sortbysupply());
                break;
            case "change12":
                Collections.sort(readIn, new Sortbychange12());
                break;
            case "change24":
                Collections.sort(readIn, new Sortbychange24());
                break;
            case "change3":
                Collections.sort(readIn, new Sortbychange3());
                break;
            case "change7":
                Collections.sort(readIn, new Sortbychange7());
                break;
            case "changemonth":
                Collections.sort(readIn, new Sortbychangemonth());
                break;
            default:
        }
    }

}
