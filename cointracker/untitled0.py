# -*- coding: utf-8 -*-
"""
Created on Sat Feb 16 19:10:42 2019

@author: agray
"""
import json
import requests
import os
import datetime
import urllib.request


url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest'
headers = {'X-CMC_PRO_API_KEY' : 'fcde8aac-f5fc-49b1-91bb-220b32635e8e'}
response = requests.get(url, headers=headers)

## delete only if file exists ##
if os.path.exists("data.json"):
    os.remove("data.json")

x = response.json()

with open('data.json', 'w') as f:
    json.dump(x,f, indent = 4)
    





#get the date and time of now
now = datetime.datetime.now()

if(now.hour - 12 <= 0):
    r = int(now.hour) - 12
    newTime = now.replace(hour = 24 + r, day = now.day - 1)
else:
    newTime = now.replace(hour = now.hour - 12)

t = newTime.strftime("%Y-%m-%dT")

t = t + newTime.strftime("%H")
t = t + "%3A"
t = t + newTime.strftime("%M")
t = t + "%3A"
t = t + newTime.strftime("%SZ")

url = "https://api.nomics.com/v1/currencies/interval?key=2018-09-demo-dont-deploy-b69315e440beb145&start="
url = url + t
x = urllib.request.urlopen(url).read()

y = x.decode('utf8').replace("'", '"')

data = json.loads(y)

if(os.path.exists("12hours.json")):
    os.remove("12hours.json")

with open("12hours.json", "w") as f:
    json.dump(data,f,indent = 4)




#get the date and time of now
now = datetime.datetime.now()

if(now.day - 3 <= 0):
    r = int(now.day) - 3
    r = r * -1
    newTime = now.replace(day = 30 - r, month = now.month - 1)
else:
    newTime = now.replace(day = now.day - 4)

t = newTime.strftime("%Y-%m-%dT")

t = t + newTime.strftime("%H")
t = t + "%3A"
t = t + newTime.strftime("%M")
t = t + "%3A"
t = t + newTime.strftime("%SZ")

url = "https://api.nomics.com/v1/currencies/interval?key=2018-09-demo-dont-deploy-b69315e440beb145&start="
url = url + t
x = urllib.request.urlopen(url).read()

y = x.decode('utf8').replace("'", '"')

data = json.loads(y)

if(os.path.exists("3days.json")):
    os.remove("3days.json")

with open("3days.json", "w") as f:
    json.dump(data,f,indent = 4)


#get the date and time of now
now = datetime.datetime.now()

if(now.month - 1 <= 0):
    newTime = now.replace(year = now.year - 1, month = 12)
else:
    newTime = now.replace(month = now.month - 1)

t = newTime.strftime("%Y-%m-%dT")

t = t + newTime.strftime("%H")
t = t + "%3A"
t = t + newTime.strftime("%M")
t = t + "%3A"
t = t + newTime.strftime("%SZ")

url = "https://api.nomics.com/v1/currencies/interval?key=2018-09-demo-dont-deploy-b69315e440beb145&start="
url = url + t
x = urllib.request.urlopen(url).read()

y = x.decode('utf8').replace("'", '"')

data = json.loads(y)

if(os.path.exists("month.json")):
    os.remove("month.json")

with open("month.json", "w") as f:
    json.dump(data,f,indent = 4)
