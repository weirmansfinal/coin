/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cointracker;
import java.util.ArrayList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
/**
 *
 * @author Rachel
 */
public class Favorites {
    //need arrayList, add, remove (by ticker) (objects are Cryptos) also a write and read to file 
    
    private ArrayList<Crypto> favoritesList;
    
    public Favorites(){
        favoritesList=new ArrayList<Crypto>();
    }
    
    public ArrayList<Crypto> getFaves(){
        return favoritesList;
    }
    
    public void add(Crypto c){
        favoritesList.add(c);
    }
    
    public void remove(Crypto c){
        favoritesList.remove(c);
    }
    
    public void writeToFile() throws FileNotFoundException, IOException{
        System.out.println("function called...");
        FileWriter f = new FileWriter("C:\\Users\\agray\\OneDrive\\Documents\\NetBeansProjects\\CoinTracker\\src\\cointracker\\favorites.json");
        JSONArray list=new JSONArray();
        for (Crypto coin:favoritesList){
            System.out.println("writing one coin...");
            JSONObject jsonCoin=new JSONObject();
            jsonCoin.put("name", coin.name);
            jsonCoin.put("ticker",coin.ticker);
            jsonCoin.put("volume",coin.volume.toString());
            jsonCoin.put("price",coin.price.toString());
            jsonCoin.put("supply",coin.supply);
            jsonCoin.put("change12",coin.change12);
            jsonCoin.put("change24",coin.change24);
            jsonCoin.put("change3",coin.change3);
            jsonCoin.put("change7",coin.change7);
            jsonCoin.put("changemonth",coin.changemonth);
            
            list.add(jsonCoin);
            
        }
        f.write(list.toJSONString());
        f.flush();
        System.out.println("Done Writing");
        
    }
    
    public void readFromFile() throws FileNotFoundException, IOException, ParseException{
        try{
            JSONParser parser=new JSONParser();
        Object obj = parser.parse(new FileReader("C:\\Users\\agray\\OneDrive\\Documents\\NetBeansProjects\\CoinTracker\\src\\cointracker\\favorites.json"));
        JSONArray arr = (JSONArray) obj;
      
        
        for (Object o:arr){
            Crypto coin=new Crypto();
            JSONObject jsonCoin=(JSONObject) o;
            
            String name = (String) jsonCoin.get("name");
            coin.name = name;

            String symbol = (String) jsonCoin.get("ticker");
            coin.ticker = symbol;

            BigDecimal volume = new BigDecimal(jsonCoin.get("volume").toString());
            coin.volume = volume;
            
            BigDecimal price = new BigDecimal(jsonCoin.get("price").toString());
            coin.price = price;
            
            double supply = new Double(jsonCoin.get("supply").toString());
            coin.supply = supply;

            double change12 = (double) jsonCoin.get("change12");
            coin.change12 = change12;
            
            double daychange = (double) jsonCoin.get("change24");
            coin.change24 = daychange;

            double threehrchange = (double) jsonCoin.get("change3");
            coin.change3 = threehrchange;
            
            double weekchange = (double) jsonCoin.get("change7");
            coin.change7 = weekchange;
            
            double monthchange = (double) jsonCoin.get("changemonth");
            coin.changemonth = monthchange;
            
            favoritesList.add(new Crypto(coin));
        }
        }catch (FileNotFoundException f){
            new File("C:\\Users\\agray\\OneDrive\\Documents\\NetBeansProjects\\CoinTracker\\src\\cointracker\\favorites.json");
        }catch (IOException s){
        }
        
        
    }
    
    public String toString(){
        String toReturn="";
        for (Crypto c:favoritesList){
            toReturn+=c.toString()+"\n\n";
        }
        return toReturn;
    }
    
}
