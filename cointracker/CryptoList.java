/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cointracker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author logan
 */
public class CryptoList {

    ArrayList<InvestCrypto> crypList = new ArrayList<InvestCrypto>();

    CryptoList() {
        crypList = new ArrayList<InvestCrypto>();
    }

    public ArrayList<InvestCrypto> getCrypList() {
        return crypList;
    }

    public void setCrypList(ArrayList<InvestCrypto> crypList) {
        this.crypList = crypList;
    }

    public void add(InvestCrypto in) {
        this.crypList.add(in);
    }

    public void Delete(String id) {
        for (int i = 0; i < crypList.size(); i++) {
            //if it is in the list delete else do nothing 
            if (crypList.get(i).getID().equals(id)) {
                crypList.remove(i);
            }
        }

    }

    public void edit(String id, int n) {
        for (int i = 0; i < crypList.size(); i++) {
            //if in the list chage else do nothing
            if (crypList.get(i).getID().equals(id)) {
                //if edit to zero delete
                if (n == 0) {
                    crypList.remove(i);
                } //if edit to negative quantity
                else if (n < 0) {
                    //do nothing
                } else {
                    crypList.get(i).setAmount(n);
                }
            }
        }
    }

    public void fileOut() throws IOException {
        JSONObject obj;
        FileWriter f = new FileWriter("C:\\Users\\agray\\OneDrive\\Documents\\NetBeansProjects\\CoinTracker\\src\\cointracker\\investments.json");
        JSONArray list = new JSONArray();
        for (int i = 0; i < crypList.size(); i++) {
            obj = new JSONObject();

            obj.put("purchasID", crypList.get(i).getID());
            obj.put("coinName", crypList.get(i).coin.name);
            obj.put("coinTicker", crypList.get(i).coin.ticker);
            obj.put("coinPrice", crypList.get(i).coin.price);
            obj.put("amount", crypList.get(i).getAmount());

            list.add(obj);

        }

        f.write(list.toJSONString());
        f.flush();
        
        System.out.println("The file was written to");
    }

    public void filein() throws IOException, ParseException {

        try {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader("C:\\Users\\agray\\OneDrive\\Documents\\NetBeansProjects\\CoinTracker\\src\\cointracker\\investments.json"));
            JSONArray arr = (JSONArray) obj;
            InvestCrypto t = new InvestCrypto();
            for (Object o : arr) {
                JSONObject jsonCoin = (JSONObject) o;
                
                
                t.coin = new Crypto();
                BigDecimal r = new BigDecimal(jsonCoin.get("coinPrice").toString());
                t.CurrentPrice = r;
                
                String coinTick = (String) jsonCoin.get("coinTicker");
                t.coin.ticker = coinTick;
                
                long e = (long) jsonCoin.get("amount");
                t.amount = (int)e;

                String ID = (String) jsonCoin.get("purchasID");
                t.PurchID = ID;

                String coinName = (String) jsonCoin.get("coinName");
                t.coin.name = coinName;

                crypList.add(new InvestCrypto(t));
            }
        } catch (IOException t) {
            new File("C:\\Users\\jwagg\\Documents\\coin\\cointracker\\investments.json");
        }

    }
}
