/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cointracker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleScriptContext;
import org.python.util.PythonInterpreter;

/**
 *
 * @author agray
 */
public class CoinTracker extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        //Load the Fxml file into a rool var
        Parent root = FXMLLoader.load(getClass().getResource("CoinScrum3TabsFXML.fxml"));

        //use the parent var to create a new scene with the name of this class
        Scene scene = new Scene(root);
        stage.setTitle("Coin Tracker");

        //show the scene
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ScriptException, InterruptedException {

        String t = "C:\\Users\\agray\\OneDrive\\Documents\\NetBeansProjects\\CoinTracker\\src\\cointracker\\readIn.py";
        String command = "cmd.exe /c start " + t;
        
        Process p = Runtime.getRuntime().exec(command);
        
        TimeUnit.SECONDS.sleep(3);
        launch(args);
    }

    public static void Sort(String a, ArrayList<Crypto> cryptoList) {
        switch (a) {
            case "name":
                Collections.sort(cryptoList, new Sortbyname());
                break;
            case "ticker":
                Collections.sort(cryptoList, new Sortbyticker());
                break;
            case "volume":
                Collections.sort(cryptoList, new Sortbyvolume());
                break;
            case "price":
                Collections.sort(cryptoList, new Sortbyprice());
                break;
            case "supply":
                Collections.sort(cryptoList, new Sortbysupply());
                break;
            case "change12":
                Collections.sort(cryptoList, new Sortbychange12());
                break;
            case "change24":
                Collections.sort(cryptoList, new Sortbychange24());
                break;
            case "change3":
                Collections.sort(cryptoList, new Sortbychange3());
                break;
            case "change7":
                Collections.sort(cryptoList, new Sortbychange7());
                break;
            case "changemonth":
                Collections.sort(cryptoList, new Sortbychangemonth());
                break;
            default:
        }
    }

}

class Sortbyname implements Comparator<Crypto> {

    @Override
    public int compare(Crypto o1, Crypto o2) {
        return o1.compareToName(o2);
    }
}

class Sortbyticker implements Comparator<Crypto> {

    @Override
    public int compare(Crypto o1, Crypto o2) {
        return o1.compareToTicker(o2);
    }
}

class Sortbyvolume implements Comparator<Crypto> {

    @Override
    public int compare(Crypto o1, Crypto o2) {
        BigDecimal a = o1.volume.subtract(o2.volume);
        return a.compareTo(new BigDecimal(0));
    }
}

class Sortbyprice implements Comparator<Crypto> {

    @Override
    public int compare(Crypto o1, Crypto o2) {
        BigDecimal a = o1.price.subtract(o2.price);
        return a.compareTo(new BigDecimal(0));
    }
}

class Sortbysupply implements Comparator<Crypto> {

    @Override
    public int compare(Crypto o1, Crypto o2) {
        return o1.compareToSupply(o2);
    }
}

class Sortbychange12 implements Comparator<Crypto> {

    @Override
    public int compare(Crypto o1, Crypto o2) {
        return o1.compareToChange12(o2);
    }
}

class Sortbychange24 implements Comparator<Crypto> {

    @Override
    public int compare(Crypto o1, Crypto o2) {
        return o1.compareToChange24(o2);
    }
}

class Sortbychange3 implements Comparator<Crypto> {

    @Override
    public int compare(Crypto o1, Crypto o2) {
        return o1.compareToChange3(o2);
    }
}

class Sortbychange7 implements Comparator<Crypto> {

    @Override
    public int compare(Crypto o1, Crypto o2) {
        return o1.compareToChange7(o2);
    }
}

class Sortbychangemonth implements Comparator<Crypto> {

    @Override
    public int compare(Crypto o1, Crypto o2) {
        return o1.compareToChangeMonth(o2);
    }

}
